CREATE table memo_data (
    id  integer primary key autoincrement NOT NULL,
    user_id  integer unique NOT NULL,
    memo_1  varchar(300),
    memo_2  varchar(300),
    memo_3  varchar(300),
    memo_4  varchar(300),
    memo_5  varchar(300),
    foreign key(user_id) references user_data (id)
);