require 'rdbi'       # RDBIを使う
require 'rdbi-driver-sqlite3' # RDBIでSQLite3と接続するドライバ
#require './memo_data_manager.rb'

class UserDataManager
  # UserDataManagerクラスのインスタンスを初期化する
  def initialize( sqlite_name )
    @db_name = sqlite_name
    @dbh = RDBI.connect( :SQLite3, :database => @db_name )
#    @self_name = :user_data_manager
  end
  
  
  def list_all_user_data(is_skip = false)
    unless is_skip
      # テーブル上の項目名を日本語に変えるハッシュテーブル
      item_name = {:name => "ユーザー名"}
      puts "\n-----ユーザーの表示-----"
      # テーブルからデータを読み込んで表示する
      sth = @dbh.execute("select name from user_data")
      # テーブルの項目名を配列で取得
      columns = sth.schema.columns.to_a
      puts "\n---------------"
      # select文の実行結果を1件ずつrowに取り出し、繰り返し処理する
      counts = 0
      sth.each do |row|
        # rowは1件分のデータを保持している
        row.each_with_index do |val, index|
          puts "#{item_name[columns[index].name]}: #{val.to_s}"
        end
        puts "---------------"
        counts = counts + 1
      end
      # 実行結果を解放する
      sth.finish
      puts "\n#{counts}件表示しました。"
      puts "\n使用するユーザー名を入力してください。"
      print "\nユーザー名: "
      user_name = gets.chomp
      print "\nエンターキーを押して～"
      line = gets
    end
    run
  end
  
  
  def run
    while true
      # 機能選択画面を表示する
      print "
1. 難易度選択画面へ
2. メモ管理画面へ
番号を選んでください(1,2)："
      # 文字の入力を待つ
      num = gets.chomp
      case
      when '1' == num
        # データベースとの接続を終了する
        @dbh.disconnect
        # 難易度選択画面へ
        return :mode_manager
#        ret = ModeManager.new("game01.db").run2
#        unless @self_name == ret
#          return ret
#        end
      when '2' == num
        # データベースとの接続を終了する
        @dbh.disconnect
        # メモ管理画面へ
        return :memo_manager
#        ret = MemoDataManager.new("game01.db").run
#        unless @self_name == ret
#          return ret
#        end
      else
        # 処理選択待ち画面に戻る
      end
    end
  end
  
#クラスのend
end

#user_data_manager = UserDataManager.new("game01.db")
#user_data_manager.list_all_user_data

if __FILE__ == $0
 user_data_manager = UserDataManager.new("game01.db")
 #ユーザーのSQLite3のデータベースを指定
 user_data_manager.list_all_user_data

end
