# -*- coding: utf-8 -*-

require 'rdbi'
require 'rdbi-driver-sqlite3'
require 'date'

class ChoiceGame
  def initialize( sqlite_name )
    #SQLiteデータベースに接続
    @db_name = sqlite_name
    @dbh = RDBI.connect( :SQLite3, :database => @db_name)
    puts "@dbhの中身:#{@dbh}"
    @event_id = 2
#   @self_name = :choice_game
  end


  def run
    while true
#      move_top = false;
      # 質問を主題する
      sth = @dbh.execute("select * from event where id = ?", @event_id)
      # テーブルの項目名を配列で取得
      columns = sth.schema.columns.to_a
      sth.each do |row|
        choice_set = {}
      # :rowは1件分のデータを保持している
        row.each_with_index do |val, index|
          if :text == columns[index].name
              puts "■〓■〓■〓■〓■〓■〓■〓■〓■〓■〓■〓■〓■
       ∧,,∧      
    （=・ω・）
   （,, ｕｕﾉ
*…*…*…*…*…*…*…*…*…*…*…*…*…*…*…*…               "
              puts " #{columns[index].name}: #{val.to_s}"  
          end
        end
        puts "*…*…*…*…*…*…*…*…*…*…*…*…*…*…*…*…"
        puts "\n"
    
        # 選択肢が表示される
        sth = @dbh.execute("select * from choice where event_id = ?", @event_id)
        # テーブルの項目名を配列で取得
        columns = sth.schema.columns.to_a
        sth.each do |row|
        # :rowは1件分のデータを保持している
        choice_row = {}
=begin
          num = nil
          text = nil
          next_event_id = nil
          row.each_with_index do |val, index|
            if :text == columns[index].name
              text = val.to_s
            elsif :num == columns[index].name
              num = val.to_s
            elsif :next_event_id == columns[index].name
              next_event_id = val.to_s
            end
          end
          puts "#{num}:#{text}"
          choice_set[num] = next_event_id
=end
          row.each_with_index do |val, index|
            choice_row[columns[index].name] = val.to_s
          end
          puts "#{choice_row[:num]}:#{choice_row[:text]}"
          choice_set[choice_row[:num]] = choice_row[:next_event_id]
        end
        puts "\n"
        puts "*…*…*…*…*…*…*…*…*…*…*…*…*…*…*…*…"
        puts "\n"
        puts "番号を選んでね⇒"
        if choice_set.empty?
          puts "---------------------
1.場所選択へ戻る
2.トップページへ戻る
3.メモ
---------------------"
          num = gets.chomp

          case num
          when '1'
            @event_id = 2
          when '2'
#            move_top = true
            return :game_manager
          when '3'
            return :memo_manager
#            move_top = true
#            ret = MemoDataManager.new("game01.db").run
#            unless @self_name == ret
#              return ret
#            end
#            return
            
          end
        else
          # 選択肢を選ぶ
       
          input = gets.chomp
          gets.chomp = not null
          # 選択肢の判定
          @event_id = choice_set[input]  
        end
      end
#puts "move_top=#{move_top}" 
#      break if move_top
    end
  end
end

if __FILE__ == $0
  choice_game = ChoiceGame.new("game01.db")

  # 処理の選択と選択後の処理を繰り返す
  choice_game.run
end
