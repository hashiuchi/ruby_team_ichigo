require 'rdbi'       # RDBIを使う
require 'rdbi-driver-sqlite3' # RDBIでSQLite3と接続するドライバ

class UserData
  #UserDataクラスのインスタンスを初期化
  def initialize ( name )
    @name = name
  end

  attr_accessor :name

  def to_s
    "#{@name}"
  end

  def to_formatted_string( sep = "\n")
    "ユーザー名：#{@name}#{sep}"
  end
end
