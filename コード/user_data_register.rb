require 'rdbi'       # RDBIを使う
require 'rdbi-driver-sqlite3' # RDBIでSQLite3と接続するドライバ

class UserDataRegister
  # UserDataRegisterクラスのインスタンスを初期化する
  def initialize( sqlite_name )
    @db_name = sqlite_name
    @dbh = RDBI.connect( :SQLite3, :database => @db_name )
#    @self_name = :user_data_register
  end


  def add_user_data
    puts "\n-----ユーザーの登録-----"
    print "ユーザーを登録します。"

    
    # テーブル上の項目名を日本語に変えるハッシュテーブル
    item_name = {:name => "ユーザー名"}
    puts "\n"
    puts "\n既に登録されているユーザー"
    # テーブルからデータを読み込んで表示する
    sth = @dbh.execute("select name from user_data")
    # テーブルの項目名を配列で取得
    columns = sth.schema.columns.to_a
    puts "---------------"
    # select文の実行結果を1件ずつrowに取り出し、繰り返し処理する
    counts = 0
    sth.each do |row|
      #rowは1件分のデータを保持している
      row.each_with_index do |val, index|
        puts "#{item_name[columns[index].name]}: #{val.to_s}"
      end
      puts "---------------"
      counts = counts + 1
    end
    # 実行結果を解放する
    sth.finish


    # ユーザーデータ1件分のインスタンスを作成する
    user_data = UserData.new( "" )
    # 登録するデータを項目ごとに入力する
    puts "\n上記以外のユーザー名を入力して下さい。"
    print "ユーザー名: "
    user_data.name = gets.chomp
    puts "\nユーザー名を「#{user_data.name}」で登録しますか？(Y/yなら登録します)"
    yesno = gets.chomp.upcase
      if /^Y$/ =~ yesno
        # 作成したユーザーデータを1件分をデータベースに登録する
        @dbh.execute("insert into user_data (name) values (?)",
                     user_data.name)
        puts "\n登録しました。"
        puts "\nエンターキーを押して難易度選択画面へ"
        line = gets
=begin
    ret = nil
    ret = ModeManager.new("game01.db").run2
    unless @self_name == ret
      return ret
    end
=end
        #難易度選択画面遷移
        @dbh.disconnect
        return( :mode_manager)
      else
        puts "\nトップページへ戻りました。"
        @dbh.disconnect
        return :game_manager
      end
  end
  
#クラスのend
end

#ユーザーのSQLite3のデータベースを指定
#user_data_register = UserDataRegister.new("test01.db")
#user_data_register.add_user_data

if __FILE__ == $0
 user_data_register = UserDataRegister.new("game01.db")
 user_data_register.add_user_data
end
