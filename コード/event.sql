drop table if exists event;
CREATE table event (
    id  integer  primary key  autoincrement  NOT NULL,
    text  varchar(500)
    );



insert into event (text) values('しっぽ君が帰ってこない');
insert into event (text) values('飼い猫のしっぽ君が３日も帰ってこない
   そろそろ探しに行こう！
   どこを探す？');
insert into event (text) values('どの公園を探す？' );
insert into event (text) values('運動公園のどこを探す？');
insert into event (text) values('ランニングコースには居なかった
   ２ｋｍコースを三週もしたのに      
   しっぽ君は見つからなかった GAME OVER');
insert into event (text) values('子供遊具場にはいなかった
   ネコのデザインの遊具があって可愛かった
   しっぽ君は見つからなかった GAME OVER');                                   
insert into event (text) values('子供公園のどこを探す？');
insert into event (text) values('滑り台の上にはネコがいたが しっぽくんではなかった
   しっぽ君は見つからなかった GAME OVER');
insert into event (text) values('ブランコが風で揺れているだけだった ブランコに乗って遊んでみた
   全力で漕いでるのを小学生に見られて恥ずかしかった
   しっぽ君は見つからなかった GAME OVER');                                   
insert into event (text) values('猫が居る公園ので猫に話しかける？');
insert into event (text) values('ちょっと何言ってるか分からない。
   手がかりはなかった。
   しっぽ君は見つからなかった GAME OVER');
insert into event (text) values('他を当たろう
   しっぽ君は見つからなかった GAME OVER');
insert into event (text) values('商店街で誰から情報を集める？');
insert into event (text) values('魚屋のおっちゃんに何を聞く?');
insert into event (text) values('おっちゃん「いやー見てないね、それより今日はこの魚が安いよ！」
   ネコの情報で はなく安い魚の情報が得られた
   しっぽ君は見つからな かった GAME OVER');
insert into event (text) values('おっちゃん「今日はカツオがオススメだよ！買っていってくれるのかい？なら一匹サービスするよ！」
   ただの買い物になってしまった。
   家に帰る？');
insert into event (text) values('お母さん「お帰り。あら、魚を買ってきてくれたのね！今日の晩御飯はそのお魚にしましょ！」
   仕方ない、明日探しに行くか
   しっぽ君は見つからなかった GAME OVER');
insert into event (text) values('歩いていると後ろから何かの気配を感じる  振り返るとネコが後を付けていた。
   なんとそれはしっぽくんだった！
   しっぽ君を見つけた！ CLEAR');
insert into event (text) values('おっちゃん「ちょっと！兄ちゃん！そんなところに突っ立ってると商売の邪魔だよ！」
   おっちゃんに怒られてしまった。  怖くなってそのばを去った。
   しっぽ君は見つから なかった GAME OVER');
insert into event (text) values('パン屋のお姉さんに何を聞く?');
insert into event (text) values('お姉さん「ん〜見てないわね。魚屋さんとか言ってみたらどう？」
   魚屋に向かう');
insert into event (text) values('お姉さん「そうよ！焼きたてだから是非買っていってね！」 新作のパンを買って、食べた。
   最高に美味しかった！今度友達にも教えよう！
   しっぽ君は見つからなかった GAME OVER');
insert into event (text) values('お姉さん「そうね〜、どこかって、いったいどこへ行きたいの？」
   お姉さんとどこへ行く？');
insert into event (text) values('お姉さんと楽しい時間を過ごすことができた！今度は休みの日に映画を見に行く約束をした！
   しっぽ君は見つからなかった GAME OVER');
insert into event (text) values('お姉さん「ノープランで誘ったの？もーちゃんと考えてないと駄目じゃない」                
   お姉さんをガッカリさせてしまった  落ち込んだので家に帰った
   しっぽ君は見つからなかった GAME OVER');                              
insert into event (text) values('八百屋のおばさんに何を聞く?');
insert into event (text) values('おばさん「猫なら、さっき店の前を歩いて行ったよ！確か商店街の入り口の方に向かっていったよ」
   入り口の方に向かったがそこにしっぽくんはいなかった。どこかへいってしまったようだ。
   しっぽ君は見つからなかった GAME OVER');
insert into event (text) values('おばさん「猫なら、さっき店の前を歩いて行ったよ！確か商店街の入り口の方に向かって行ったよ」
   入り口の方に向かったがそこにしっぽくんはいなかった。どこかへいってしまったようだ。
   しっぽ君は見つからなかった GAME OVER');
insert into event (text) values('おばさん「なんでそんなこと聞くんだい？」  おばさんに訳を話した
   おばさん「そうかい、そうかいそれは大変だねぇ。ねこは気まぐれだからね、そのうち帰ってるよ」
   おばさんに そう言わ家に帰った。
   お母さんに「ちゃんと探してきたの？もう一回探しておいで！」と怒られた
   第一選択肢に戻る');
insert into event (text) values ('おばさん「はいよ。また買いに来てね」 キャベツを買うことができて満足だ
   しっぽ君は見つからなかった GAME OVER');
insert into event (text) values('誰の家を訪ねる?');
insert into event (text) values('本田さんとゲームをする?');
insert into event (text) values('本田さんとゲームをしっぽくんを探すのを忘れて楽しんだ。
   しっぽ君は見つからなかった GAME OVER');
insert into event (text) values('他の家も訪ねてみたがしっぽくんは見つからなかった                                                                        GAME OVER');
insert into event (text) values('菅田くんの弟と遊ぶ?');
insert into event (text) values('窓の方から物音がした。窓に近づいてみる?');
insert into event (text) values('近づいてみるとそこにはしっぽくんがいた！
   しっぽ君を見つけた！ CLEAR');
insert into event (text) values('菅田くんの弟も気づき窓の方へ駆け寄る。
   菅田くんの弟「お兄ちゃん！猫がいるよ！」
   なんとその猫はしっぽくんだった！
   しっぽ君を見つけた！ CLEAR');
insert into event (text) values('家にいても仕方ない。探しに行こう！');                                   
