require 'rdbi'       # RDBIを使う
require 'rdbi-driver-sqlite3' # RDBIでSQLite3と接続するドライバ

class MemoDataManager
  # MemoDataManagerクラスのインスタンスを初期化する
  def initialize( sqlite_name )
    @db_name = sqlite_name
    @dbh = RDBI.connect( :SQLite3, :database => @db_name )
  end
  
  
   # 処理の選択と選択後の処理を繰り返す
  def run
    while true
      # 機能選択画面を表示する
      print "
1. メモの登録
2. メモの更新
3. メモの表示
4. 選択画面へ戻る
番号を選んでください(1,2,3,4)："
      # 文字の入力を待つ
      num = gets.chomp
      case
      when '1' == num
        # メモの登録
        add_memo_data
      when '2' == num
        # メモの更新
        upd_memo_data
      when '3' == num
        # メモの表示
        list_all_memo_data
      when '4' == num
        # データベースとの接続を終了する
        @dbh.disconnect
        # 選択画面へ戻る
        return :user_data_manager_from_memo
      else
        # 処理選択待ち画面に戻る
      end
    end
  end
  
  
  #メモの登録
  def add_memo_data
    puts "\n-----メモの登録-----"
    print "メモを登録します。"
    # 登録するデータの入力
    puts "\n"
    print "\nユーザー名: "
    user_name = gets.chomp
    print "\nメモ内容
"
    memo_text = gets.chomp
    #作成したユーザーデータを1件分をデータベースに登録する
    @dbh.execute("INSERT into memo_data (memo_1, user_id) values(?, (select id from user_data where name = ?))",
                  memo_text,
                  user_name)
    puts "\n登録しました。"
  end
  
  
  #メモの更新
  def upd_memo_data
    puts "\n-----メモの更新-----"
    print "メモを更新します。"
    # 登録するデータの入力
    puts "\n"
    print "\nユーザー名: "
    user_name = gets.chomp
    while true
      # 機能選択画面を表示る
      print "\nメモ番号を選んでください(1～5)："
      # 文字の入力を待つ
      num = gets.chomp
        case
        when '1' == num
          memo_num = "memo_1"
          break;
        when '2' == num
          memo_num = "memo_2"
          break;
        when '3' == num
          memo_num = "memo_3"
          break;
        when '4' == num
          memo_num = "memo_4"
          break;
        when '5' == num
          memo_num = "memo_5"
          break;
        else
          # 処理選択待ち画面に戻る
        end
    end
    print "\nメモ内容
"
    memo_text = gets.chomp
    #作成したユーザーデータを1件分をデータベースに登録する
    @dbh.execute("update memo_data set #{memo_num} = ? where user_id = (select id from user_data where name = ?)",
                  memo_text,
                  user_name)
    puts "\n更新しました。"
  end


  def list_all_memo_data
    # テーブル上の項目名を日本語に変えるハッシュテーブル
    item_name = {:memo_1 => "メモ1", :memo_2 => "メモ2",
       :memo_3 => "メモ3", :memo_4 => "メモ4", :memo_5 => "メモ5" }
    puts "\n-----メモデータの表示-----"
    print "メモデータを表示します。"
    puts "\n"
    print "\nユーザー名: "
    user_name = gets.chomp
    puts "\n---------------"
    # テーブルからデータを読み込んで表示する
    sth = @dbh.execute("select memo_1, memo_2, memo_3, memo_4, memo_4 from memo_data where user_id = (select id from user_data where name = ?)", user_name)
    # テーブルの項目名を配列で取得
    columns = sth.schema.columns.to_a
    # select文の実行結果を1件ずつrowに取り出し、繰り返し処理
    sth.each do |row|
      # rowは1件分のデータを保持している
      row.each_with_index do |val, index|
        puts "#{item_name[columns[index].name]}: #{val.to_s}"
      end
      puts "--------------"
    end
    # 実行結果を解放する
    sth.finish
  end

#クラスのend
end


if __FILE__ == $0
 memo_data_manager = MemoDataManager.new("game01.db")
 #ユーザーのSQLite3のデータベースを指定
 memo_data_manager.run
end

